from flask import Flask, jsonify
from controllers.record_controller import record_api

app = Flask(__name__)

app.register_blueprint(record_api)

if __name__ == '__name__':
    app.run(debug= True)
