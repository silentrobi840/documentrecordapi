from datetime import datetime

class Record:

    def __init__(self,
                 title=None,
                 description=None,
                 images=None,
                 **kwargs):
        self.title = title
        self.description = description
        self.images = images
