from datetime import datetime
import core.globals.global_objects as _global

REQUIRED_FIELDS = ['title', 'description']

def is_required_record_fields_present(dictionary):
    for key in REQUIRED_FIELDS:
        if key in dictionary and dictionary[key] == None:
            return False
    return True

class RecordService:

    def create_record(self, record):
        record = record.__dict__

        if (is_required_record_fields_present(record) == False):
            return (False, f"Missing required fields. Make sure to include {REQUIRED_FIELDS} fields")

        record['createdAt'] = datetime.now()
        record['updatedAt'] = datetime.now()
        # we will add actual createdBy , updatedBy when we will add authentication and authorization
        record['createdBy'] = 1
        record['updatedBy'] = None

        try:
            _global.repository_unit.record_repository.create_record(record)
        except:
            return (False, "Failed to insert new record")
        return (True, None)

    def get_records(self):
        try:
            return (True, _global.repository_unit.record_repository.get_record_list())
        except:
            return (False, "Something went wrong while reading records!")
    
    def get_record(self, id):
        try:
            record = _global.repository_unit.record_repository.get_record(id)

            if (record):
                return (record, None)
            else:
                return (None, "No record is found!")
        except:
            return (None, "Something went wrong while reading record!")

    def delete_record(self, id):
        try:
            delete_summary = _global.repository_unit.record_repository.delete_record(id)

            if (delete_summary.deleted_count > 0):
                return (True, "Record is deleted successfully")
            else:
                return (False, "No record is found to delete")
        except:
            return (False, "Something went wrong while deleting record!")

    def update_record(self, id, update_record):
        update_record = update_record.__dict__
        try:
            record = _global.repository_unit.record_repository.get_record(id)
            if (record):
                del record['_id']
                for key in update_record.keys():
                    if (update_record[key] != None):
                        record[key] = update_record[key]

                record['updatedAt'] = datetime.now()
                record['updatedBy'] = 1

                return (True, "Record is updated successfully")
            else:
                return (False, "Failed to update!")
        except:
            return (False, "Something went wrong while updating record!")
