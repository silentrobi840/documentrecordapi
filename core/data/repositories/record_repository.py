
from core.data.db_connection import db_connect  # import from parent level
from bson.objectid import ObjectId


class RecordRepository:

    def __init__(self):
        self.db = db_connect()

    def create_record(self, record):
        self.db.record.insert_one(record)

    def get_record_list(self):
        cursor = self.db.record.find()
        records = []
        for record in cursor:
            record['_id'] = str(record['_id'])  # This does the trick!
            records.append(record)
        return records

    def update_record(self, id, record):
        self.db.record.update_one({'_id': ObjectId(id)}, {"$set": record})

    def get_record(self, id):
        record = self.db.record.find_one({'_id': ObjectId(id)})
        if (record):
            record['_id'] = str(record['_id'])
        return record

    def delete_record(self, id):
        return self.db.record.delete_one({'_id': ObjectId(id)})
