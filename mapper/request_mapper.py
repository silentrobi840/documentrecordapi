from flask import request
from functools import wraps


def request_mapping(cls):
    def wrap(f):
        @wraps(f)
        def decorator(*args, **kwargs):
            if(request.data):
                obj = cls(**request.get_json())  # map json to class object
                return f(obj, *args, **kwargs)
            else:
                return f(cls({}), *args, **kwargs)
        return decorator
    return wrap
