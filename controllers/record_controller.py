from flask import Blueprint, jsonify
from core.models.record import Record
import mapper.request_mapper as map
import core.globals.global_objects as _global
record_api = Blueprint('record_api', __name__)

@record_api.route("/api/v1/records", methods=['GET'])
def get_records():
    isSucceed, result = _global.record_service.get_records()
    
    if (isSucceed):
          return (jsonify(result), 200)
    else:
        return (result, 400)


@record_api.route("/api/v1/records", methods=['POST'])
@map.request_mapping(Record)
def create_records(record):
    isSucceed, error = _global.record_service.create_record(record)
    if (isSucceed):
        return ("new record created successfully", 201)
    else:
        return (error, 400)


@record_api.route("/api/v1/records/<id>", methods=['GET'])
def get_record(id):
    result, error = _global.record_service.get_record(id)
    if (error):
        return ("No record is found", 400)
    return (result, 200)


@record_api.route("/api/v1/records/<id>", methods=['DELETE'])
def delete_record(id):
    isSucceed, message = _global.record_service.delete_record(id)
    if(isSucceed):
        return (message, 200)
    else:
        return (message, 400)


@record_api.route("/api/v1/records/<id>", methods=['PUT'])
@map.request_mapping(Record)
def update_record(record, id):
    isSucceed, message = _global.record_service.update_record(id, record)
    if(isSucceed):
        return (message, 200)
    else:
        return (message, 400)